##base url:a.1.7cong.cn

# 期刊

## 1 获取期刊方向选项

get /periodical/getDirectionOptions

#### 参数
无

#### 返回
    {
        err_no : 0,
        data : [
            id : 0,
            data : "综合类"
        ],
        ...
    }
#### 错误
    {
        err_no: 001,
        err_msg: "接口错误，请稍后再试"
    }

***
## 2 获取期刊周期选项
get /periodical/getCycleOptions
#### 参数
无

#### 返回
    {
        err_no : 0,
        data : [
            id : 0,
            data : "周刊"
        ],
        ...
    }
#### 错误
    {
        err_no: 001,
        err_msg: "接口错误，请稍后再试"
    }
***
## 3 获取期刊审核标准选项
get /periodical/getAuditStandardOptions
#### 参数
无

#### 返回
    {
        err_no : 0,
        data : [
            id : 0,
            data : "B级"
        ],
        ...
    }
#### 错误
    {
        err_no: 001,
        err_msg: "接口错误，请稍后再试"
    }    
***
## 4 获取期刊被收录数据库选项
get /periodical/getIncludedDatabaseOptions
#### 参数
无

#### 返回
    {
        err_no : 0,
        data : [
            id : 0,
            data : "知网"
        ],
        ...
    }
#### 错误
    {
        err_no: 001,
        err_msg: "接口错误，请稍后再试"
    }
***
## 5 获取期刊级别选项
get /periodical/getLevelOptions
#### 参数
无

#### 返回
    {
        err_no : 0,
        data : [
            id : 0,
            data : "省级"
        ],
        ...
    }
#### 错误
    {
        err_no: 001,
        err_msg: "接口错误，请稍后再试"
    }    
***
## 6 获取期刊列表
get /periodical/list
#### 参数
|参数名|必填|示例|备注|
|:----:|:----:|:----:|:----:|
|keyword|否|----|搜索关键字|
|direction|否|[1,2]|方向选项id数组|
|included_database|否|[1,2]|被收录数据库id数组|
|level|否|[1,2]|期刊等级id数组|
|cycle|否|[1,2]|期刊周期id数组|
|year|否|[2017,2018]|收录年份数组|
|page|否|[100,300]|页码范围数组|
|audit_standard|否|[1,2]|审核标准数组|
|invoice|否|0，1|是否开发票，0否1是|
|suspend_receiving|否|0，1|是否暂停收稿，0否1是|
|science|否|0，1|是否是一二批学术期刊，0否1是|
|post_code|否|0，1|是否邮发刊号|
|e_number|否|0，1|是否电子刊号|
|check|否|0，1|是否可查稿|
|cover_core|否|0，1|是否封面核心|
|impact_factor|否|0，1|是否影响因子|
|factor_range|否|[0.1,0.2,0.3]|影响因子区间|
|date_y|否|2018|出刊时间-年|
|date_m|否|02|出刊时间-月|
|date_type|否|1,2,3,4,5|出刊时间-月初、上旬、中旬、下旬、月底|



#### 返回
    {
        err_no : 0,
        data : [
            total : 100, //总条数
            data : [
                {
                    id: 1,
                    name: "少年科普报",
                    cover: "http://xxxx.png",
                    audit_standard_id: 1,
                    audit_standard: "C级",
                    direction: [
                        "综合类",
                        "教育类",
                    ],
                    level_id: 1,
                    level: "省级",
                    cycle_id: 1,
                    cycle: "周刊",
                    date: "2021-03",
                    date_type_id: 1,
                    date_type: "月初",
                    //included_database_id: 1,
                    included_database: "龙源",
                    current: "0315少年科普报xxxxx",
                    letter: "http://xxxx.pdf",
                },
                ...
            ]
        ],
        ...
    } 
   

    
***
## 7 获取期刊基本信息
get /periodical/baseInfo
#### 参数
|参数名|必填|示例|备注|
|:----:|:----:|:----:|:----:|
|id|是|1|期刊id|




#### 返回
    {
        err_no : 0,
        data : {
             id: 1,
             name: "少年科普报",
             included_database_id: 1,
             included_database: "龙源",
             char_number: "2500字符/1版",
             remark: "",//附件信息
             level_id: 1,
             level: "省级",
             price: [   //价格 未登录时不返回
                {
                    id:1,
                    role_id: 1,
                    role: "大代理",
                    price: "950元/3版（超过6000字符按千字符收费）"
                },
                ...
             ]
             current: "0315少年科普报xxxxx",
        }
    } 
    


***
## 8 获取期刊详细信息
get /periodical/detail
#### 参数
|参数名|必填|示例|备注|
|:----:|:----:|:----:|:----:|
|id|是|1|期刊id|




#### 返回
    {
        err_no : 0,
        data : {
            id: 1,
            name: "少年科普报",
            cover: "http://xxxx.png",
            audit_standard_id: 1,
            audit_standard: "C级",
            direction: [
                "综合类",
                "教育类",
            ],
            level_id: 1,
            level: "省级",
            cycle_id: 1,
            cycle: "周刊",
            date: "2021-03",
            date_type_id: 1,
            date_type: "月初",
            included_database: ["龙源","知网"],
            current: "0315少年科普报xxxxx",
            letter: "http://xxxx.pdf",
            science_periodical: "第一批",
            history: [
                {
                    id: 2,
                    date: xxxxx //timestamp
                    data: "03.08 通信电源技术 收2020年12下（24期） 预计2021年4月中上旬左右出刊；2021年1下（2期） 预计2021年4月中上旬左右出刊"
                },
                ...
            ],
            price: [
                {
                    id: 1,
                    role_id: 1,
                    role: "大代理",
                    price: "950元/3版（超过6000字符按千字符收费）"
                }
            ],
            domestic_number: "42-1380/TN",
            international_number: "1009-3664",
            post_code: "38-371",
            char_count:  "2500字符/1版",
            organizer: "抚顺日报",
            management: "抚顺日报",
            cover_core: 0,
            impact_factor: 0,
            simple: "一篇文章1本样刊,多一本20元",
            page: "398",
            page_remark: "固定页码",
            postcode: "可要可不要",
            summary_keywords: "可要可不要",
            author_info: "可要可不要",
            references: "可要可不要",
            review_difficulty: "一般",
            review_time: "1天",
            column: "主要栏目:科学探索、教研视点、信息技术、幼儿教育、小学教育、中学教育、高等教育等"
            remark: "",
            favorite: '0'

        }
    } 
    


    
## 9 当前期刊

get /periodical/current

#### 参数
|参数名|必填|示例|备注|
|:----:|:----:|:----:|:----:|
|page|是|1||
|limit|是|10||



#### 返回
    {
        err_no : 0,
        data : [
            {
                id: 1,
                data: "03.15 少年科普报（科教论坛） 现在收2021年8期 2021年3月底左右出刊 本周截稿"
            },
            ...
        ]
    }    
  
      
## 10 收藏期刊

get /periodical/favorite

#### 参数
|参数名|必填|示例|备注|
|:----:|:----:|:----:|:----:|
|id|是|1|期刊id|
|type|是|1|0 取消收藏 1 添加收藏|



#### 返回
    {
        err_no : 0,
        data : [
            {
                id: 1,
                data: "03.15 少年科普报（科教论坛） 现在收2021年8期 2021年3月底左右出刊 本周截稿"
            },
            ...
        ]
    }       

***
***
***

# 用户

## 1 登录

post /user/login

#### 参数
|参数名|必填|示例|备注|
|:----:|:----:|:----:|:----:|
|phone|是|-|登录手机号|
|password|是|-|密码|



#### 返回
    {
        err_no : 0,
        data : [
            'access_token' : " xxxx",
            'token_type' : 'bearer',
            'expires_in' : 3600
        ]
       
    }



## 2 用户信息

get /user/info

#### 参数
无


#### 返回
    {
        err_no : 0,
        data : [
            name: "xxx",
            role: '普通用户',
            phone: 186xxxx
        ]
        
    }


## 3 修改密码

post /user/resetPassword

#### 参数
|参数名|必填|示例|备注|
|:----:|:----:|:----:|:----:|
|old_password|是|-|旧密码|
|new_password|是|-|新密码|



#### 返回
    {
        err_no : 0,
        data : true,
    }

## 4 登出

get /user/logout

#### 参数
无


#### 返回
    {
        err_no : 0,
        data : true
    }



## 5 刷新token

post /user/refresh

#### 参数
无


#### 返回
    {
        err_no : 0,
        data : [
            'access_token' : " xxxx",
            'token_type' : 'bearer',
            'expires_in' : 3600
        ],
    }


## 6 注册

post /user/register

#### 参数
|参数名|必填|示例|备注|
|:----:|:----:|:----:|:----:|
|name|是|-|用户名|
|phone|是|-|电话|
|password|是|-|密码|



#### 返回
    {
        err_no : 0,
        data : {
            session_id : xxxxx
        },
    }
    
    

## 7 注册验证

post /user/verification

#### 参数
|参数名|必填|示例|备注|
|:----:|:----:|:----:|:----:|
|session_id|是|-||
|code|是|-|验证码|




#### 返回
       {
            err_no : 0,
            data : [
                'access_token' : " xxxx",
                'token_type' : 'bearer',
                'expires_in' : 3600
            ],
            ...
        }


    

## 8 收藏列表

get /user/favorite_list

#### 参数
|参数名|必填|示例|备注|
|:----:|:----:|:----:|:----:|
|page|是|-||
|limit|是|-||



#### 返回
    {
           err_no : 0,
           data : [
               total : 100, //总条数
               data : [
                   {
                       id: 1,
                       name: "少年科普报",
                       cover: "http://xxxx.png",
                       audit_standard_id: 1,
                       audit_standard: "C级",
                       direction: [
                           "综合类",
                           "教育类",
                       ],
                       level_id: 1,
                       level: "省级",
                       cycle_id: 1,
                       cycle: "周刊",
                       date: "2021-03",
                       date_type_id: 1,
                       date_type: "月初",
                       //included_database_id: 1,
                       included_database: "龙源",
                       current: "0315少年科普报xxxxx",
                       letter: "http://xxxx.pdf",
                   },
                   ...
               ]
           ],
           ...
       } 


## 9 绑定代理关系 

post /user/bindAgent

#### 参数
|参数名|必填|示例|备注|
|:----:|:----:|:----:|:----:|
|code|是|-|代理邀请码|




#### 返回
       {
            err_no : 0,
            data : true,
        }

## 10 微信登录

post /weApp/login

#### 参数
|参数名|必填|示例|备注|
|:----:|:----:|:----:|:----:|
|code|是|-|we.login返回的code|
|nickname|是|-|we.getuserinfo返回的nickname|
|country|是|-|we.getuserinfo返回的country|
|province|是|-|province|
|city|是|-|city|
|gender|是|-|gender|
|language|是|-|language|

#### 返回
        //该用户绑定过微信
       {
              err_no : 0,
              data : [
                  'status' : 'success',
                  'access_token' : " xxxx",
                  'token_type' : 'bearer',
                  'expires_in' : 3600
              ]
             
       }
       //该用户未绑定微信或未注册,需要调用loginByPhone
        {
            err_no: 0,
            data : [
                status : fail,
                session_id : XXX
        }
        
        
## 10 微信登录 byphone

post /weApp/loginByPhone

#### 参数
|参数名|必填|示例|备注|
|:----:|:----:|:----:|:----:|
|session_id|是|-|login返回的session_id|
|iv|是|-|wx.getPhoneNumber返回的iv|
|encryptedData|是|-|wx.getPhoneNumber返回的encryptedData|


#### 返回
        //该手机号注册过，登录成功
       {
              err_no : 0,
              data : [
                  'status' : 'success',
                  'access_token' : " xxxx",
                  'token_type' : 'bearer',
                  'expires_in' : 3600
              ]
             
       }
       //该手机号未注册,需要调用setPassword
        {
            err_no: 0,
            data : [
                status : fail,
                session_id : XXX
        }

        
## 11 设置密码

post /weApp/setPassword

#### 参数
|参数名|必填|示例|备注|
|:----:|:----:|:----:|:----:|
|session_id|是|-|login返回的session_id|
|password|是|-|用户设置的密码|


#### 返回
        //登录成功
       {
              err_no : 0,
              data : [
                  'status' : 'success',
                  'access_token' : " xxxx",
                  'token_type' : 'bearer',
                  'expires_in' : 3600
              ]
       }
     

#其他
## 1 提交咨询工单

post /advisory

#### 参数
|参数名|必填|示例|备注|
|:----:|:----:|:----:|:----:|
|name|是|-|用户名|
|email|否|-||
|phone|否|-||
|content|否|-|内容|




#### 返回
       {
            err_no : 0,
            data : true,
        }






## 2 获取出刊统计列表

get /publishStatus

#### 参数


#### 返回
       {
            err_no : 0,
            data : [
                {
                    'date': '2021/05',
                    'count': 100
                },
                ...
            ],
        }
        


# 错误

    {
        err_no: x,
        err_msg: x
    }
    
     $err = [
                '001' => '接口错误，请稍后再试',
                '002' => '参数错误',
                '003' => '该手机号已注册，请直接登录',
                '004' => '手机号或密码错误',
                '005' => '密码错误',
                '006' => '未找到该期刊',
                '007' => '该期刊已被收藏',
                '008' => '该期刊未被收藏',
                '009' => '验证码错误',
            ];
