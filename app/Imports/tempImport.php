<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToArray;
use Maatwebsite\Excel\Concerns\ToCollection;

class tempImport implements ToCollection
{

    /**
     * @param  Collection  $collection
     */
    public function collection(Collection $collection)
    {
        $levelData = DB::table('levels')
            ->get();

        $pData = collect([]);
        $priceData = collect([]);
        $collection = $collection->slice(1);
        $pid = 1;
        $collection->map(function ($item) use (&$pData, $levelData, &$priceData, &$pid) {
            try {
                $pData[] = [
                    'id' => $pid,
                    'name' => $item[0],
                    'level_id' => $levelData->firstWhere('name', $item[1])->id,
                    'char_count' => $item[4],
                    'column' => $item[5]
                ];
                $priceData[] = [
                    'periodical_id' => $pid,
                    'price' => 0,
                    'role_id' => 1
                ];
                $priceData[] = [
                    'periodical_id' => $pid,
                    'price' => 0,
                    'role_id' => 2
                ];
                $priceData[] = [
                    'periodical_id' => $pid,
                    'price' => 0,
                    'role_id' => 3
                ];
                $priceData[] = [
                    'periodical_id' => $pid,
                    'price' => 0,
                    'role_id' => 4
                ];
                $pid++;
            } catch (\Exception $e) {
                dd($item);
            }
        });
        DB::table('periodicals')
            ->insert($pData->toArray());
        DB::table('periodical_prices')
            ->insert($priceData->toArray());

    }
}
