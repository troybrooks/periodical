<?php

namespace App\Http\Controllers;

use App\Models\Options\AuditStandard;
use App\Models\Options\Cycle;
use App\Models\Options\Direction;
use App\Models\Options\IncludedDatabase;
use App\Models\Options\Level;
use App\Models\Options\PublicationTime;
use Illuminate\Http\Request;

class PeriodicalOptionController extends Controller
{
    //
    public function getDirectionOptions(){
        $data = Direction::orderBy('sort')->get();
        return api_output($data);
    }

    public function getCycleOptions(){
        $data = Cycle::all();
        return api_output($data);
    }

    public function getAuditStandardOptions(){
        $data = AuditStandard::all();
        return api_output($data);
    }

    public function getLevelOptions(){
        $data = Level::all();
        return api_output($data);
    }

    public function getIncludedDatabaseOptions(){
        $data = IncludedDatabase::all();
        return api_output($data);
    }
    public function getPublicationOptions(){
        $data = PublicationTime::all();
        return api_output($data);
    }
}
