<?php

namespace App\Http\Controllers;

use App\Models\Advisory;
use Illuminate\Http\Request;

class AdvisoryController extends Controller
{
    //
    public function insert(){
        $validator = validator(\request()->all(), [
            'name' => 'required|string',
            'content' => 'required|string'
        ]);
        if ($validator->fails()) {
            return api_error('002');
        }

        $name = \request('name');
        $email = \request('email');
        $phone = \request('phone');
        $content = \request('content');

        if(is_null($email) && is_null($phone)){
            return api_error('002');
        }

        $advisory = new Advisory;
        $advisory->name = $name;
        $advisory->email = $email;
        $advisory->phone = $phone;
        $advisory->content = $content;
        $advisory->save();

        return api_output(true);
    }
}
