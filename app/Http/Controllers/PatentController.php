<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PatentController extends Controller
{
    //
    public function getList(){
        $validator = validator(\request()->all(), [
            'page' => 'required|integer',
            'limit' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return api_error('002');
        }

        $page = \request('page');
        $limit = \request('limit');

        $data = DB::table('patent')
            ->orderBy('sort','desc')
            ->forPage($page, $limit)
            ->get();

        $count = DB::table('patent')
            ->count();

        return api_output([
            'count' => $count,
            'data' => $data
        ]);
    }
    
    public function staff(){
        $validator = validator(\request()->all(), [
            'page' => 'required|integer',
            'limit' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return api_error('002');
        }

        $page = \request('page');
        $limit = \request('limit');

        $data = DB::table('users')
            ->where('role_id','>=',4)
            ->orderBy('sort','desc')
            ->forPage($page, $limit)
            ->get();

        $count = DB::table('users')
            ->where('role_id','>=',4)
            ->count();

        return api_output([
            'count' => $count,
            'data' => $data
        ]);
    }
}
