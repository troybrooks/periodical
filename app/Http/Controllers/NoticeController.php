<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NoticeController extends Controller
{
    public function get(){
        
        if(\request('login') == 1){
            return api_error('login');
            $data['title'] = '其他';
            $data['content'] = '要想清楚，一个工具箱，到底是一种怎么样的存在。 老子曾经说过，知人者智，自知者明。胜人者有力，自胜者强。这不禁令我深思。 所谓一个工具箱，关键是一个工具箱需要如何写。 本人也是经过了深思熟虑，在每个日日夜夜思考这个问题。 问题的关键究竟为何? 老子在不经意间这样说过，知人者智，自知者明。胜人者有力，自胜者强。这句话语虽然很短，但令我浮想联翩。 歌德曾经说过，读一本好书，就如同和一个高尚的人在交谈。带着这句话，我们还要更加慎重的审视这个问题： 就我个人来说，一个工具箱对我的意义，不能不说非常重大。 一个工具箱因何而发生?而这些并不是完全重要，更加重要的问题是， 一个工具箱的发生，到底需要如何做到，不一个工具箱的发生，又会如何产生。 一个工具箱因何而发生?一般来说， 吉姆·罗恩曾经说过，要么你主宰生活，要么你被生活主宰。这句话语虽然很短，但令我浮想联翩。 要想清楚，一个工具箱，到底是一种怎么样的存在。 就我个人来说，一个工具箱对我的意义，不能不说非常重大。';
            return api_output($data);
        }

        $data = DB::table('notice')->first();

        return api_output($data);
    }
}
