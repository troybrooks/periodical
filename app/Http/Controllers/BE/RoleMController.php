<?php


namespace App\Http\Controllers\BE;


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class RoleMController
{
    public function getList()
    {
        $user = auth()->user();
        if ($user->role_id !== 5 && $user->role_id !== 6) {
            return api_error('012');
        }
        $validator = validator(\request()->all(), [
            'page' => 'required|integer',
            'limit' => 'required|integer',
            'keyword' => 'nullable|string',
        ]);
        if ($validator->fails()) {
            return api_error('002');
        }

        $input_role_id = request('role_id',4);
        $keyword = request('keyword');
        $query = DB::table('users as u')
            ->leftJoin('roles as r', 'u.role_id', '=', 'r.id')
            ->whereIn('role_id', [2, 3, 4,5,6])
            ->when($user->role_id ==4,function($query)use($user){
                return $query->where('u.id', $user->id);
            })
            ->selectRaw('u.id, u.name, role_id, r.name as role_name, u.phone, u.avatar, u.level,u.sort')
            ->when($keyword, function($query, $keyword){
                return $query->where('u.name', 'like', "%$keyword%")->orwhere('u.phone', 'like', "%$keyword%");
            })
            ->when($input_role_id == 4,function($query,$input_role_id){
                return $query->whereIn('role_id', [4,5,6]);
            })
            ->when($input_role_id == 3,function ($query,$input_role_id){
                return $query->where('role_id',3);
            })
            ->when($input_role_id == 2,function ($query,$input_role_id){
                return $query->where('role_id',2);
            })
            ->when($input_role_id == 1,function ($query,$input_role_id){
                return $query->where('role_id',1);
            });

        $count = $query->count();
        $data = $query
            ->orderBy('sort','desc')
            ->forPage(request('page'), request('limit'))
            ->get();


        return api_output([
            'data' => $data,
            'count' => $count
        ]);
    }

    public function getRoleOptions(){
        $data = DB::table('roles')
            ->where('id', '<', 6)
            ->get();
        return api_output($data);
    }

    public function setRole(){
        $user = auth()->user();
        if ($user->role_id !== 5 && $user->role_id !== 6) {
            return api_error('012');
        }
        $validator = validator(\request()->all(), [
            'role_id' => 'required|integer',
            'user_id' => 'required|integer',

        ]);
        if ($validator->fails()) {
            return api_error('002');
        }

        $data = DB::table('users')
            ->where('id', request('user_id'))
            ->update([
                'role_id' => request('role_id'),
                'operator' => auth()->id(),
                'updated_at' => Carbon::now()
            ]);
        return api_output(true);
    }

    public function updateInfo(){
        $user = auth()->user();
        if ($user->role_id !== 5 && $user->role_id !== 6) {
            return api_error('012');
        }
        $validator = validator(\request()->all(), [
            'id' => 'required|integer',
            'name' => 'required',
            'phone' => 'required|nullable',
        ]);
        if ($validator->fails()) {
            return api_error('002');
        }
        if($user->role_id ==4 && ($user->id != \request('id') || \request('level') != null)){
            return api_error('012');
        }



        DB::table('users')
            ->where('id', \request('id'))
            ->update([
                'name' => \request('name'),
                'phone' => \request('phone'),
                'level' => \request('level'),
                'operator' => $user->id,
                'updated_at' => Carbon::now()
            ]);

        return api_output(true);
    }

    public function updateAvatar(){
        $user = auth()->user();
        if ($user->role_id !== 5 && $user->role_id !== 6) {
            return api_error('012');
        }
        $validator = validator(\request()->all(), [
            'id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return api_error('002');
        }
        if($user->role_id ==4 && $user != request('id')){
            return api_error('012');
        }

        $path = Storage::putFile('file', request()->file('file'));
        DB::table('users')
            ->where('id', request('id'))
            ->update([
                'avatar' =>  \request()->root().'/storage/'.$path,
                'operator' => $user->id,
                'updated_at' => Carbon::now()
            ]);

        return api_output(true);
    }
    
    public function updateSort()
    {
        $user = auth()->user();
        if ($user->role_id !== 5 && $user->role_id !== 6) {
            return api_error('012');
        }

        $id = request('id');
        $sort = request('sort');

        DB::beginTransaction();
        try {
            DB::table('users')
                ->where('id',$id)
                ->update(['sort'=>$sort]);
            
            DB::commit();
            return api_output(true);
        } catch (\Exception $e) {
            Log::error('更新排序出错'.$e->getMessage());
            return api_error('013');
        }
    }
}
