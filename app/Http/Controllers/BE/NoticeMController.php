<?php

namespace App\Http\Controllers\BE;

use App\Http\Controllers\Controller;
use App\Models\Periodical;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class NoticeMController extends Controller
{
    public function get(){

        $data = DB::table('notice')->first();

        return api_output($data);
    }

    public function update(){
        $validator = validator(\request()->all(), [
            'title' => 'required|string',
            'content' => 'required|string'
        ]);
        if ($validator->fails()) {
            return api_error('002');
        }

        $user = auth()->user();
        if ($user->role_id !== 5 && $user->role_id !== 4 && $user->role_id !== 6) {
            return api_error('012');
        }

        DB::table('notice')
            ->update([
                'title' => \request('title'),
                'content' => \request('content'),
                'sort' => \request('sort'),
            ]);

        return api_output(true);
    }
}
