<?php

namespace App\Http\Controllers\BE;

use App\Http\Controllers\Controller;
use App\Models\Periodical;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PatentMController extends Controller
{
    //
    public function uploadFile()
    {
        $user = auth()->user();
        if ($user->role_id !== 5 && $user->role_id !== 4 && $user->role_id !== 6) {
            return api_error('012');
        }

        if(request()->file('file') == null){
            return api_error('002');
        }

        // config('periodical.file_update_base_url')
        $path = \request()->root().'/storage/'.Storage::disk('public')->putFile('file', request()->file('file'),'public');

        $res['errno'] = 0;
        $data['url'] = $path;
        $data['alt'] = '';
        $data['href'] = $path;
        $res['data'] = [$data];
        return json_encode($res);
    }

    public function updateFile()
    {
        $user = auth()->user();
        if ($user->role_id !== 5 && $user->role_id !== 4 && $user->role_id !== 6) {
            return api_error('012');
        }

        if(request()->file('file') == null){
            return api_error('002');
        }

        // config('periodical.file_update_base_url')
        $path = \request()->root().'/storage/'.Storage::disk('public')->putFile('file', request()->file('file'),'public');

        return api_output($path);
    }

    public function getList(){
        $validator = validator(\request()->all(), [
            'limit' => 'required|integer',
            'page' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return api_error('002');
        }

        $page = \request('page');
        $limit = \request('limit');

        $data = DB::table('patent')
            ->forPage($page, $limit)
            ->get();

        $count = DB::table('patent')
            ->count();

        return api_output([
            'count' => $count,
            'data' => $data
        ]);
    }



    public function add(){
        $validator = validator(\request()->all(), [
            'img' => 'required|string',
            'descript' => 'required|string',
            'sort' => 'required|string',
        ]);
        if ($validator->fails()) {
            return api_error('002');
        }

        $user = auth()->user();
        if ($user->role_id !== 5 && $user->role_id !== 4 && $user->role_id !== 6) {
            return api_error('012');
        }

        DB::table('patent')
            ->insert([
                'img' => \request('img'),
                'descript' => \request('descript'),
                'sort' => \request('sort'),
            ]);

        return api_output(true);
    }

    public function update(){
        $validator = validator(\request()->all(), [
            'img' => 'required|string',
            'descript' => 'required|string',
            'sort' => 'required|string',
            'id' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return api_error('002');
        }

        $user = auth()->user();
        if ($user->role_id !== 5 && $user->role_id !== 4 && $user->role_id !== 6) {
            return api_error('012');
        }

        DB::table('patent')
            ->where('id', \request('id'))
            ->update([
                'img' => \request('img'),
                'descript' => \request('descript'),
                'sort' => \request('sort'),
            ]);

        return api_output(true);
    }

    public function delete(){
        $validator = validator(\request()->all(), [
            'id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return api_error('002');
        }

        $user = auth()->user();
        if ($user->role_id !== 5 && $user->role_id !== 4 && $user->role_id !== 6) {
            return api_error('012');
        }
        DB::table('patent')
            ->where('id', \request('id'))
            ->delete();
        return api_output(true);
    }
}
