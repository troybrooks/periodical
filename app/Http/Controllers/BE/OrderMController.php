<?php


namespace App\Http\Controllers\BE;


use Illuminate\Support\Facades\DB;

class OrderMController
{
    public function getList(){
        $user = auth()->user();
        if ($user->role_id !== 5 && $user->role_id !== 6) {
            return api_error('012');
        }
        $validator = validator(\request()->all(), [
            'page' => 'required|integer',
            'limit' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return api_error('002');
        }
        $data = DB::table('advisories')
            ->orderByDesc('id')
            ->forPage(request('page'), request('limit'))
            ->get();

        $count = DB::table('advisories')->count();

        return api_output([
            'data' => $data,
            'count' => $count
        ]);
    }

    public function read(){
        $user = auth()->user();
        if ($user->role_id !== 5 && $user->role_id !== 6) {
            return api_error('012');
        }
        $validator = validator(\request()->all(), [
            'id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return api_error('002');
        }
        DB::table('advisories')
            ->where('id', request('id'))
            ->update([
                'is_read' => 1
            ]);
        return api_output(true);
    }
}
