<?php

namespace App\Http\Controllers\BE;

use App\Http\Controllers\Controller;
use App\Models\Periodical;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ArticleMController extends Controller
{
    //

    public function updateFile()
    {
        $user = auth()->user();
        if ($user->role_id !== 5 && $user->role_id !== 4 && $user->role_id !== 6) {
            return api_error('012');
        }

        if(request()->file('file') == null){
            return api_error('002');
        }

        $path = \request()->root().'/storage/'.Storage::putFile('file', request()->file('file'));

        return api_output($path);
    }

    public function getList(){
        $validator = validator(\request()->all(), [
            'limit' => 'required|integer',
            'page' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return api_error('002');
        }

        $page = \request('page');
        $limit = \request('limit');

        $data = DB::table('articles')
            ->forPage($page, $limit)
            ->get();

        $count = DB::table('articles')
            ->count();

        return api_output([
            'count' => $count,
            'data' => $data
        ]);
    }



    public function add(){
        $validator = validator(\request()->all(), [
            'cover' => 'required|string',
            'link' => 'required|string',
            'title' => 'required|string',
        ]);
        if ($validator->fails()) {
            return api_error('002');
        }

        $user = auth()->user();
        if ($user->role_id !== 5 && $user->role_id !== 4 && $user->role_id !== 6) {
            return api_error('012');
        }

        DB::table('articles')
            ->insert([
                'cover' => \request('cover'),
                'link' => \request('link'),
                'title' => \request('title'),
            ]);

        return api_output(true);
    }

    public function update(){
        $validator = validator(\request()->all(), [
            'cover' => 'required|string',
            'link' => 'required|string',
            'title' => 'required|string',
            'id' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return api_error('002');
        }

        $user = auth()->user();
        if ($user->role_id !== 5 && $user->role_id !== 4 && $user->role_id !== 6) {
            return api_error('012');
        }

        DB::table('articles')
            ->where('id', \request('id'))
            ->update([
                'cover' => \request('cover'),
                'link' => \request('link'),
                'title' => \request('title'),
            ]);

        return api_output(true);
    }

    public function delete(){
        $validator = validator(\request()->all(), [
            'id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return api_error('002');
        }

        $user = auth()->user();
        if ($user->role_id !== 5 && $user->role_id !== 4 && $user->role_id !== 6) {
            return api_error('012');
        }
        DB::table('articles')
            ->where('id', \request('id'))
            ->delete();
        return api_output(true);
    }
}
