<?php

namespace App\Http\Controllers\BE;

use App\Http\Controllers\AuthController;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends AuthController
{
    //
    public function login(){
        $validator = validator(\request()->all(), [
            'phone' => 'required|numeric',
            'password' => 'required|string'
        ]);
        if ($validator->fails()) {
            return api_error('002');
        }

        $phone = \request('phone');
        $password = \request('password');

        $user = User::where('phone', $phone)
            ->where('password', hash('sha256', $password))
            ->with('role')
            ->first();

        if (is_null($user)) {
            return api_error('004');
        }
        if($user->role_id == 1){
            return api_error('012');
        }

        $token = auth()->login($user);

        return $this->respondWithToken($token);
    }
}
