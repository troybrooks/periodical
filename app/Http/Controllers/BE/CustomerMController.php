<?php


namespace App\Http\Controllers\BE;


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CustomerMController
{
    public function getList()
    {
        $validator = validator(\request()->all(), [
            'page' => 'required|integer',
            'limit' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return api_error('002');
        }

        $keyword = request('keyword');
        $user = auth()->user();

        $query = DB::table('users as u')
            ->leftJoin('users as a', 'u.agent_id', '=', 'a.id')
            ->where('u.role_id', 1)
            ->when($user->role_id !== 6 && $user->role_id !== 5, function ($query) use ($user) {
                return $query->where('u.agent_id', auth()->id());
            })
            ->when($keyword, function ($query, $keyword) {
                return $query->where('u.name', 'like', "%{$keyword}%")->orWhere('u.phone', 'like', "%{$keyword}%");
            })
            ->selectRaw('u.*, a.name as agent_name');
        $count = $query->count();
        $data = $query->forPage(request('page'), request('limit'))
            ->get();


        return api_output([
            'data' => $data,
            'count' => $count
        ]);
    }

    public function getAgentOptions()
    {
        $data = DB::table('users')
            ->whereIn('role_id', [2, 3])
            ->selectRaw('id, name')
            ->get();
        return api_output($data);
    }

    public function setAgent()
    {
        $validator = validator(\request()->all(), [
            'agent_id' => 'required|integer',
            'customer_id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return api_error('002');
        }
        $user = auth()->user();
        if ($user->role_id !== 5 && $user->role_id !== 6) {
            return api_error('012');
        }

        DB::table('users')
            ->where('id', request('customer_id'))
            ->update([
                'agent_id' => request('agent_id')
            ]);
        return api_output(true);
    }

    public function setRole(){
        $user = auth()->user();
        if ($user->role_id !== 5 && $user->role_id !== 6) {
            return api_error('012');
        }
        $validator = validator(\request()->all(), [
            'role_id' => 'required|integer',
            'user_id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return api_error('002');
        }

        $data = DB::table('users')
            ->where('id', request('user_id'))
            ->update([
                'role_id' => request('role_id'),
                'operator' => auth()->id(),
                'updated_at' => Carbon::now()
            ]);
        return api_output(true);
    }

    public function updateInfo(){
        $user = auth()->user();
        if ($user->role_id !== 5 && $user->role_id !== 6) {
            return api_error('012');
        }
        $validator = validator(\request()->all(), [
            'id' => 'required|integer',
            'name' => 'required',
            'phone' => 'required|nullable',
            'country' => 'nullable',
            'province' => 'nullable',
            'city' => 'nullable',
            'gender' => 'nullable',
        ]);
        if ($validator->fails()) {
            return api_error('002');
        }

        if(request('id')){
            DB::table('users')
            ->where('id', request('id'))
            ->update([
                'name' => request('name'),
                'phone' => request('phone'),
                'country' => request('country'),
                'province' => request('province'),
                'city' => request('city'),
                'remark' => request('remark'),
                'gender' => request('gender'),
                'operator' => $user->id,
                'updated_at' => Carbon::now()
            ]);
        }
        else{
            $is_exist = DB::table('users')->where('phone',request('phone'))->first();
            if($is_exist){
                DB::table('users')
                ->where('id', $is_exist->id)
                ->update([
                    'name' => request('name'),
                    'phone' => request('phone'),
                    'gender' => request('gender'),
                    'password' => hash('sha256', '123456'),
                    'operator' => $user->id,
                    'updated_at' => Carbon::now()
                ]);
                return api_output(true);
            }
            // 新增用户
            DB::table('users')->insert([
                'name' => request('name'),
                'phone' => request('phone'),  
                'gender' => request('gender'),
                'operator' => $user->id,
                'password' => hash('sha256', '123456'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        return api_output(true);

    }
    
    public function deleteUser(){
        $id = request('id');
        DB::table('users')->where('id',$id)->delete();
        
        return api_output(true);
    }

    public function updateAvatar(){
        $user = auth()->user();
        if ($user->role_id !== 5 && $user->role_id !== 6) {
            return api_error('012');
        }
        $validator = validator(\request()->all(), [
            'id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return api_error('002');
        }
        $path = Storage::putFile('file', request()->file('file'));
        DB::table('users')
            ->where('id', request('id'))
            ->update([
                'avatar' =>  \request()->root().'/storage/'.$path,
                'operator' => $user->id,
                'updated_at' => Carbon::now()
            ]);

        return api_output(true);
    }
}
