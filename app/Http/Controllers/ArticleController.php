<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller
{
    //
    public function getList(){
        $validator = validator(\request()->all(), [
            'page' => 'required|integer',
            'limit' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return api_error('002');
        }

        $page = \request('page');
        $limit = \request('limit');

        $data = DB::table('articles')
            ->forPage($page, $limit)
            ->get();

        $count = DB::table('articles')
            ->count();

        return api_output([
            'count' => $count,
            'data' => $data
        ]);
    }
}
