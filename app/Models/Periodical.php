<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Periodical extends Model
{
    use HasFactory;

    public function directions()
    {
        return $this->belongsToMany('App\Models\Options\Direction');
    }

    public function included_databases()
    {
        return $this->belongsToMany('App\Models\Options\IncludedDatabase');
    }

    public function audit_standard()
    {
        return $this->belongsTo('App\Models\Options\AuditStandard');
    }

    public function level()
    {
        return $this->belongsTo('App\Models\Options\Level');
    }

    public function cycle()
    {
        return $this->belongsTo('App\Models\Options\Cycle');
    }

    public function publication_time()
    {
        return $this->belongsTo('App\Models\Options\PublicationTime');
    }

    public function history()
    {
        // 20220809 修改为内容开头的日期排序(就是当天填写资料的日期)
        return $this->hasMany('App\Models\PublicationHistory')->orderByDesc('created_at');
    }

    public function current()
    {
        // 20220809 修改为内容开头的日期排序(就是当天填写资料的日期)
        return $this->hasOne('App\Models\PublicationHistory', 'periodical_id', 'id')->orderByDesc('created_at');
            // ->orderByDesc('publish_at')->orderByDesc('publish_at_about');
    }

    public function price()
    {
        return $this->hasMany('App\Models\PeriodicalPrice')->where('role_id', '<=', Auth::user()->role_id)->where('role_id','<>',2)->where('role_id','<>',3);
    }
    
    public function sort_price()
    {
        return $this->hasOne('App\Models\PeriodicalPrice')->where('role_id',4);
    }

    public function favorite()
    {
        return $this->belongsToMany('App\Models\User', 'favorite', 'periodical_id', 'user_id');
    }
}
