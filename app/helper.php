<?php


if (!function_exists('api_output')) {
    function api_output($payload)
    {
        return [
            'err_no' => 0,
            'data' => $payload,
        ];
    }
}

if (!function_exists('api_error')) {
    function api_error(string $code, string $msg = null)
    {
        $err = [
            '001' => '接口错误，请稍后再试',
            '002' => '参数错误',
            '003' => '该手机号已注册，请直接登录',
            '004' => '手机号或密码错误',
            '005' => '密码错误',
            '006' => '未找到该期刊',
            '007' => '该期刊已被收藏',
            '008' => '该期刊未被收藏',
            '009' => '验证码错误',
            '010' => '该账号已绑定过代理商',
            '011' => '未找到对应代理商',
            '012' => '暂无权限进行该操作',
            '013' => '数据更新失败',
            '014' => '该手机号未注册，请联系管理员开通'
        ];

        return [
            'err_no' => $code,
            'err_msg' => is_null($msg) && isset($err[$code]) ? $err[$code] : $msg,
        ];
    }
}
