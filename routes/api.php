<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PeriodicalController;
use App\Http\Controllers\PeriodicalOptionController;
use App\Http\Controllers\AdvisoryController;
use App\Http\Controllers\WeChatController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\BE\UserController;
use App\Http\Controllers\BE\PeriodicalMController;
use App\Http\Controllers\BE\CustomerMController;
use App\Http\Controllers\BE\OrderMController;
use App\Http\Controllers\BE\RoleMController;
use App\Http\Controllers\BE\ArticleMController;
use App\Http\Controllers\BE\PatentMController;
use App\Http\Controllers\BE\NoticeMController;
use App\Http\Controllers\PatentController;
use App\Http\Controllers\NoticeController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group([
    'middleware' => 'api',
    'prefix' => 'user'
], function () {
    Route::post('register', [AuthController::class, 'register']);
    Route::post('verification', [AuthController::class, 'registerStep2']);
    Route::post('login', [AuthController::class, 'login'])->name('login');
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('resetPassword', [AuthController::class, 'resetPassword']);
    Route::get('info', [AuthController::class, 'me']);
    Route::post('bindAgent', [AuthController::class, 'bindAgent']);
});

Route::post('weApp/login', [WeChatController::class, 'login']);
Route::post('weApp/loginByPhone', [WeChatController::class, 'loginByPhone']);
Route::post('weApp/setPassword', [WeChatController::class, 'setPassword']);


Route::post('advisory', [AdvisoryController::class, 'insert']);

Route::get('publishStatus', [PeriodicalController::class, 'publishStatus']);

Route::get('articles', [ArticleController::class, 'getList']);
Route::get('patent', [PatentController::class, 'getList']);
Route::get('staff', [PatentController::class, 'staff']);
Route::get('notice', [NoticeController::class, 'get']);

Route::group(['prefix' => 'periodical'], function () {

    //search options
    Route::get('getDirectionOptions', [PeriodicalOptionController::class, 'getDirectionOptions']);
    Route::get('getCycleOptions', [PeriodicalOptionController::class, 'getCycleOptions']);
    Route::get('getAuditStandardOptions', [PeriodicalOptionController::class, 'getAuditStandardOptions']);
    Route::get('getLevelOptions', [PeriodicalOptionController::class, 'getLevelOptions']);
    Route::get('getIncludedDatabaseOptions', [PeriodicalOptionController::class, 'getIncludedDatabaseOptions']);
    Route::get('getPublicationOptions', [PeriodicalOptionController::class, 'getPublicationOptions']);

    //get data
    Route::get('list', [PeriodicalController::class, 'getList']);
    Route::get('baseInfo', [PeriodicalController::class, 'baseInfo']);
    Route::get('current', [PeriodicalController::class, 'current']);

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('detail', [PeriodicalController::class, 'detail']);
        Route::get('favorite', [PeriodicalController::class, 'favorite']);
        Route::get('favoriteList', [PeriodicalController::class, 'favoriteList']);
    });

});


Route::group(['prefix' => 'management'], function () {
    Route::group(['prefix' => 'user'], function () {
        Route::post('login', [UserController::class, 'login']);
    });
    Route::group(['prefix' => 'user', 'middleware' => 'auth:api'], function(){
        Route::get('info', [UserController::class, 'me']);
        Route::post('logout', [UserController::class, 'logout']);
    });
    Route::group(['prefix' => 'periodical', 'middleware' => 'auth:api'], function(){
        Route::get('detail', [PeriodicalMController::class, 'detail']);
        Route::get('history', [PeriodicalMController::class, 'history']);
        Route::post('updateDetail', [PeriodicalMController::class, 'updateDetail']);
        Route::post('updatePrice', [PeriodicalMController::class, 'updatePrice']);
        Route::post('updateStatus', [PeriodicalMController::class, 'updateStatus']);
        Route::post('updateSort', [PeriodicalMController::class, 'updateSort']);
        Route::post('updateFile', [PeriodicalMController::class, 'updateFile']);
        Route::post('updateHistory', [PeriodicalMController::class, 'updateHistory']);
        Route::post('deleteHistory', [PeriodicalMController::class, 'deleteHistory']);
        Route::post('delete', [PeriodicalMController::class, 'delete']);
        Route::post('addHistory', [PeriodicalMController::class, 'addHistory']);
        Route::post('add', [PeriodicalMController::class, 'add']);
        Route::get('list', [PeriodicalMController::class, 'getList']);
    });

    Route::group(['prefix' => 'customer', 'middleware' => 'auth:api'], function(){
        Route::get('list', [CustomerMController::class, 'getList']);
        Route::get('getAgentOptions', [CustomerMController::class, 'getAgentOptions']);
        Route::post('setAgent', [CustomerMController::class, 'setAgent']);
        Route::post('setRole', [CustomerMController::class, 'setRole']);
        Route::post('updateInfo', [CustomerMController::class, 'updateInfo']);
        Route::post('updateAvatar', [CustomerMController::class, 'updateAvatar']);
        Route::post('deleteUser', [CustomerMController::class, 'deleteUser']);
    });


    Route::group(['prefix' => 'order', 'middleware' => 'auth:api'], function(){
        Route::get('list', [OrderMController::class, 'getList']);
        Route::post('read', [OrderMController::class, 'read']);
    });

    Route::group(['prefix' => 'role', 'middleware' => 'auth:api'], function(){
        Route::get('list', [RoleMController::class, 'getList']);
        Route::get('getRoleOptions', [RoleMController::class, 'getRoleOptions']);
        Route::post('setRole', [RoleMController::class, 'setRole']);
        Route::post('addUser', [RoleMController::class, 'addUser']);
        Route::post('updateInfo', [RoleMController::class, 'updateInfo']);
        Route::post('updateAvatar', [RoleMController::class, 'updateAvatar']);
        Route::post('updateSort', [RoleMController::class, 'updateSort']);
    });

    Route::group(['prefix' => 'article', 'middleware' => 'auth:api'], function() {
        Route::get('list', [ArticleMController::class, 'getList']);
        Route::Post('add', [ArticleMController::class, 'add']);
        Route::Post('update', [ArticleMController::class, 'update']);
        Route::Post('delete', [ArticleMController::class, 'delete']);
        Route::Post('updateFile', [ArticleMController::class, 'updateFile']);

    });

    Route::group(['prefix' => 'patent', 'middleware' => 'auth:api'], function() {
        Route::get('list', [PatentMController::class, 'getList']);
        Route::Post('add', [PatentMController::class, 'add']);
        Route::Post('update', [PatentMController::class, 'update']);
        Route::Post('delete', [PatentMController::class, 'delete']);
        Route::Post('updateFile', [PatentMController::class, 'updateFile']);
        Route::Post('uploadFile', [PatentMController::class, 'uploadFile']);
    });
    
    Route::group(['prefix' => 'notice', 'middleware' => 'auth:api'], function() {
        Route::get('get', [NoticeMController::class, 'get']);
        Route::Post('update', [NoticeMController::class, 'update']);
    });
});
