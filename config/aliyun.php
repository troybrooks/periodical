<?php

return [
    'access_key_id' => env('ALIYUN_ACCESS_KEY_ID'),
    'access_key_secret' => env('ALIYUN_ACCESS_KEY_SECRET'),
    'access_sign' => env('ALIYUN_ACCESS_SIGN'),
    'template_code' => env('ALIYUN_ACCESS_TEMPLATE_CODE'),
];
